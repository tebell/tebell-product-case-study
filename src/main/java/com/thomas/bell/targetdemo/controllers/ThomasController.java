package com.thomas.bell.targetdemo.controllers;

import java.util.Arrays;
import java.util.Optional;

import com.thomas.bell.targetdemo.interfaces.ProductRepository;
import com.thomas.bell.targetdemo.models.Price;
import com.thomas.bell.targetdemo.models.Product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

@RestController
public class ThomasController {

    // MongoDB repo
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/products/{id}")
    public Product getProductById(@PathVariable("id") String id) {

        Optional<Product> returnProductOptional = productRepository.findById(Long.valueOf(id));

        Product returnProduct = returnProductOptional.get();

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity <String> entity = new HttpEntity<String>(headers);

        String url = "http://localhost:8081/prices/" + id;
        Price price = (Price) restTemplate.exchange(url, HttpMethod.GET, entity, Price.class).getBody();

        returnProduct.setCurrentPrice(price);


        return returnProduct;
    }
    
    @PutMapping("/products/add/{id}")
    public String addProduct(@PathVariable("id") String id, @RequestBody Product product) {

        productRepository.save(product);

        return "Success";
    }

    @PutMapping("/products/{id}")
    public String updateProductPrice(@PathVariable("id") String id, @RequestBody Product product) {

        Price price = product.getCurrentPrice();

        price.setId(product.getId());

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity <Price> entity = new HttpEntity<Price>(price, headers);

        String url = "http://localhost:8081/prices/" + id;
        String returnString = (String) restTemplate.exchange(url, HttpMethod.PUT, entity, String.class).getBody().toString();

        return returnString;
    }

    @PostMapping("/products/delete-all")
    public String deleteAllProducts() {

        productRepository.deleteAll();

        return "Success";
    }
    
}